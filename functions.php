<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	
	register_nav_menus(array('primary' => 'Primary Navigation'));

	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );
	
	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	
	/* ========================================================================================================================
	
	AJAX
	
	======================================================================================================================== */
	
	/*--Choose Division--*/
	
	function divisionSelect(){
		global $wpdb;
		$division = $_POST['division'];
		$divisionSearch = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_leaguemanager_teams WHERE league_id = '$division'"));
		foreach($divisionSearch as $team){
			echo '<option value="'.$team->id.'">'.$team->title.'</option>';
		};
		die();
	}	
	add_action('wp_ajax_divisionSelect', 'divisionSelect');
	add_action('wp_ajax_nopriv_divisionSelect', 'divisionSelect');
	
	/*--Choose Team--*/
	
	function teamSelect(){
		global $wpdb;
		$division = $_POST['division'];
		$team = $_POST['team'];
		$teamSearch = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_leaguemanager_teams WHERE id = '$team'"));
		foreach($teamSearch as $stats){
			echo '<div id="overview">
				<div id="team-overview">
					<h2>'.$stats->title.'</h2>
					<dl>
						<dt>Rank</dt>
						<dd>'.$stats->rank.'</dd>
						<dt>Matches</dt>
						<dd>'.$stats->done_matches.'</dd>
					</dl>
				</div>
				<div class="team-stats">
					<ul>
						<li>'.$stats->won_matches.' <span>Won</span></li>
						<li>'.$stats->draw_matches.' <span>Tied</span></li>
						<li>'.$stats->lost_matches.' <span>Lost</span></li>								
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			';		
		};
		$fixtureSearch = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_leaguemanager_matches WHERE away_team = '$team' AND league_id = 1")); ?>
			<div id="fixture">
				<h3>Fixture</h3>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<th>Match</th>
						<th>Date/Time</th>
						<th>Place</th>
					</tr>
					<?php foreach($fixtureSearch as $fixture){?>
						<tr>
							<td><?php
								$awayteamid = $fixture->home_team;
								$hometeam = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_leaguemanager_teams WHERE ID = '$team'"));
								$awayteam = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_leaguemanager_teams WHERE ID = '$awayteamid'"));
								foreach($hometeam as $home) { 
									echo $home->title;
								};
								echo ' VS ';
								foreach($awayteam as $away) { 
									echo $away->title;
								};
							?>
							</td>
							<td><?php echo $fixture->date; ?></td>
							<td><?php echo $fixture->location ?></td>
						</tr>
					<?php } ?>
			</table>
			</div>
			<div id="ladder">
				<h3>Ladder</h3>
				<?php leaguemanager_standings($division) ?>						
			</div>
	<?php
		die();
	}	
	add_action('wp_ajax_teamSelect', 'teamSelect');
	add_action('wp_ajax_nopriv_teamSelect', 'teamSelect');
	

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */



	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script( 'site' );

		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
	}	
		
	/* ========================================================================================================================
	
	Side Menus
	
	======================================================================================================================== */
	
	function wt_get_depth($id = '', $depth = '', $i = 0)
	{
		global $wpdb;
		global $post;
		if($depth == '')
		{
			if(is_page())
			{
				if($id == '')
				{
					$id = $post->ID;
				}
				$depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$id."'");
				return wt_get_depth($id, $depth, $i);
			}
		}
		elseif($depth == "0")
		{
			return $i;
		}
		else
		{
			$depth = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID = '".$depth."'");
			$i++;
			return wt_get_depth($id, $depth, $i);
		}
	} 
 

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}
	
	/* ========================================================================================================================
	
	Images
	
	======================================================================================================================== */
	
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'homepageCTA', 223, 128); 
	} 
	
	/* ========================================================================================================================
	
	Excerpt
	
	======================================================================================================================== */
	
	function get_the_excerpt_here($post_id)
	{
	  global $wpdb;
	  $query = "SELECT post_excerpt FROM $wpdb->posts WHERE ID = $post_id LIMIT 1";
	  $result = $wpdb->get_results($query, ARRAY_A);
	  return $result[0]['post_excerpt'];
	}