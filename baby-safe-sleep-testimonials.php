<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Blanket Reviews | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="testimonials beige page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>What people are saying</span> About BubbaCosy Wrap?</h1>
					<div id="content-copy" class="float-left">
						<ul>
							<li>
								<blockquote>..The wraps are fabulous, very happy!!!</blockquote>
								<cite>Amie<br/>Perth, WA</cite>
							</li>
							<li>
								<blockquote>A friend of ours uses your smart wraps so we bought one for our 5wks old boy. He absolutely loves it. He sleeps much better and so we. Thank you!</blockquote>
								<cite>Phill<br/>Sale , Vic</cite>
							</li>
							<li>
								<blockquote>I got my new Grand-daughter one of your wraps. I immediately loved it but I was not sure if my daughter will like it and use it. You know how these ‘young mums’ are fussy. It was excellent choice. Mum and daughter love it! My daughter found it very easy to use and so versatile. Now she wants to buy one more so she can use it all the time. Thank you.</blockquote>
								<cite>Gayle<br/>Mornington, Vic</cite>
							</li>
							<li>
								<blockquote>I bought your baby wrap for my daughter and she absolutely loves it. We cannot go anywhere without the wrap! It is sooo handy. She is snuggled all the time and I do not need to carry any more blankets. Well done! </blockquote>
								<cite>Rose<br/>Sydney, NSW</cite>
							</li>
							
							<li>
								<blockquote>We bought your baby wrap while we were in Melbourne and found it the best thing. If I order another how long before delivery we are from Adelaide.</blockquote>
								<cite>Paula Donneli<br/>Adelaide</cite>
							</li>
							<li>
								<blockquote>We just received our second baby wrap. We have one already and have used it every day. Now I can wash it:). Thank you!</blockquote>
								<cite>Amanda<br/>Brisbane, Qld</cite>
							</li>
							<li>
								<blockquote>We just bought a baby wrap for our new born baby boy. We love it! The wrap is wonderful; I never seen anything like this before. We definitely recommend it to our friends. Thank you.</blockquote>
								<cite>John &amp; Linda<br/>Braeside, Vic</cite>
							</li>
							<li>
								<blockquote>..Our baby boy Ariel has already been wrapped in your wonderful wrap. I will photograph him in a wrap and you can use it on your website. Beats the doll you have on display. Utterly beautiful Eurasian baby.</blockquote>
								<cite>Selina &amp; Russell<br/>Vic</cite>
							</li>
							<li>
								<blockquote>We love our baby wrap and our daughter sleeps more soundly - the only problem is, she has nearly outgrown it. I'm just wondering if you make them in larger sizes. We are getting ready for a Tassie winter and if you had any larger one's, I know she would be snug.</blockquote>
								<cite>JBrooke<br/>Tasmania</cite>
							</li>
							<li>
								<blockquote>I received a baby wrap as a present, and I love it! However I was wondering can you purchase covers separately without the insert. The reason that I ask is that when I wash it, I miss using it?? Cheers</blockquote>
								<cite>Linda</cite>
							</li>
							<li>
								<blockquote>I heard about your business as I was given a baby wrap and my daughter loves it. All the best.</blockquote>
								<cite>Angela<br/>Vic</cite>
							</li>
							<li>
								<blockquote>I received the gorgeous baby wrap as a gift when I gave birth to my daughter Ria. It was well admired in hospital by other mums and nurses. It is just gorgeous and will be well loved! Little baby Ria is just wonderful and I am loving being a mum!! The wraps are a wonderful idea and I have passed on your web site details to a number of my friends who are expecting! Thanks again.</blockquote>
								<cite>Anne<br/>Brighton, Vic</cite>
							</li>
							<li>
								<blockquote>I had my premmie baby in your baby wrap from when he was a couple of weeks old and only 5lb (2.2kg). My baby is now 6 months old and still loves his baby wrap - even if he grizzles as we put him in it, as soon as he is wrapped he is so comforted and calmed by the snugness of it that he is quickly off to sleep. I have 3 baby wraps so that we can rotate them in the wash; they still look like new. They are worth every penny! Thank you.</blockquote>
								<cite>Lea<br/>Caufield, Vic</cite>
							</li>
						</ul>
					</div>
					<div class="image-sing float-right">
						<div class="double-note note-one wiggle"></div>
						<div class="single-note note-two wiggle"></div>
						<div class="single-note note-three wiggle"></div>
						<div class="single-note note-four wiggle"></div>
						<div class="double-note-small note-five wiggle"></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
		<!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Wraps</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% wool doona inside, Bubba Cosy baby wraps take your baby straight from cot to pram. No need to unwrap or take extra blankets on your outing. </p>
				</div>
				<div class="float-left">
					<img src="images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->

		
		
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
