<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Wraps | Swaddle | Baby Blankets | Baby Sleeping Bags | BubbaCosy</title>
<meta name="keywords" content="baby, wrap, australia, swaddle, blankets, grow bags" />
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />

<!--//End CSS-->

</head>

<body class="home preload">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<div id="frame-header">
				<div id="header-background">
					<div class="wrapper">
					
					<!--//Navigation-->
						
							<div id="navigation-main">
								<ul class="sf-menu">
									<li id="nav-what"><a title="Discover what a BubbaCosy is"><span>What is</span> a BubbaCosy?</a>
										<ul>
											<li><a href="/about-babywraps/">What is a BubbaCosy&trade;?</a></li>
											<li><a href="/about-babywraps/how-to-use.php">As Easy as Steps 1, 2, 3!</a></li>
											<li><a href="/about-babywraps/benefits.php">Why choose BubbaCosy?</a></li>
											<li><a href="/about-babywraps/care.php">Care Instructions</a></li>
											<li><a href="/about-babywraps/history.php">The BubbaCosy&trade; Story</a></li>
										</ul>
									</li>
									<li id="nav-people"><a href="/baby-safe-sleep-testimonials.php" title="Find out what people are saying about our product"><span>What people</span> are saying?</a></li>
									<li id="nav-gallery"><a href="/baby-swaddle-gallery.php" title="View the BubbaCosy in action"><span>The wrap</span> Gallery</a></li>
									<li id="nav-stockists"><a href="/where-to-buy-baby-wraps-blankets.php" title="Find out where to buy your Bubbacosy"><span>Stockists</span> List</a></li>
									<li id="nav-shop"><a href="/baby-wrap-products.php" title="Shop Online for your own BubbaCosy now!"><span>Shop</span> Now!</a></li>
								</ul>
								<div class="clear"></div>
							</div>
							
						<!--//End Navigation-->
					
					</div>
				</div>
			</div>
			
		<!--//End Header-->
		
		<!--//Slider-->

        <div id="slider">
            <div class="logo-container">
                <h1><img src="images/logo.png" alt="BubbaCosy - The smart baby wrap" /></h1>
            </div>
            <ul class="slides">
                <li class="slide one">
                    <div class="slide-container">
                        <div class="slide-content">
                            <h2>The <span>2-in-1</span> Baby wrap</h2>
                            <p>Each BubbaCosy&trade; has a duvet inside to support posture and provide the extra warmth your baby needs. The duvet, made from 100% Australian washable wool, helps to regulate baby's body temperature.</p>
                            <a class="button" href="/baby-wrap-products.php"><span>Shop</span> now</a>
                        </div>
                    </div>
                </li>

                <li class="slide two">
                    <div class="slide-container">
                        <div class="slide-content">
                            <h2>A wrap for all seasons</h2>
                            <p>View our range of baby wraps suitable for all seasons &amp; climates. All orders include free shipping &amp; gift box.</p>
                            <a class="button" href="/baby-wrap-products.php"><span>Shop</span> now</a>
                        </div>
                    </div>
                </li>
                <li class="slide three">
                    <div class="slide-container">
                        <div class="slide-content">
                            <h2>The smart Baby wrap</h2>
                            <p>BubbaCosy&trade; has been designed to give your newborn baby the security and comfort that only a mum can give. Made from the finest quality natural fabrics it is soft and gentle on baby's skin.</p>
                            <a class="button" href="/baby-wrap-products.php"><span>Shop</span> now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
		<!--//End Splash-->
		
		<!--//Content-->
		<div class="main-content">
            <div class="top-border" id="promo-border-top"></div>
			<div class="wrapper promo"><a href="https://tickets.exponews.com.au/814"><img src="images/EBATS_MELB_Leaderboard.jpg"/></a></div>
            <div class="bottom-border" id="promo-border"></div>
            <div id="frame-modules" class="wrapper">
                <div class="why expandUp">
                    <div class="module-container">
                        <img class="why-image" src="images/baby.png" alt="">
                        <h2><span>Why choose</span> BubbaCosy wraps?</h2>
                        <p>The multi-purpose design functions as a wrap, duvet, change mat &amp; play mat. Baby is always snug and secure, encouraging a longer lasting sleep.</p>
                        <a class="button" href="/about-babywraps/benefits.php"><span>Read</span> More</a>
                    </div>
                </div>
                <div class="shop expandUp">
                    <div class="module-container">
                        <img class="shop-image" src="images/books.png" alt="">
                        <h2><span>Shop the</span> BubbaCosy range now</h2>
                        <p>View our range of baby wraps suitable for all seasons &amp; climates. All orders include free shipping &amp; gift box.</p>
                        <a class="button" href="/baby-wrap-products.php"><span>Shop</span> now</a>
                    </div>
                </div>
            </div>
			<div class="bottom-border"></div>
        </div>
		
		<!--//End Content-->

        <!--//Facebook-->
        <div class="facebook">
            <div id="frame-facebook" class="wrapper">
                <img class="facebook-img" src="images/facebook.png" alt="">
                <img class="bird-img" src="images/bird.png" alt="">
            </div>
            <a href="https://www.facebook.com/bubbacosy" target="_blank"></a>
        </div>
		
	</div>
</div>

<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
