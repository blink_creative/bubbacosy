
			<div id="frame-header">
				<div id="header-background">
					<div class="wrapper">
					
						<!--Logo-->
						
						<div id="logo-site">
							<a href="/" title="Navigate home"><img src="images/logo-site.png" alt="BubbaCosy - The smart baby wrap" /></a>
						</div>
					
						<!--//Navigation-->
						
							<div id="navigation-main">
								<ul class="sf-menu">
									<li id="nav-what"><a title="Discover what a BubbaCosy is"><span>What is</span> a BubbaCosy?</a>
										<ul>
											<li><a href="/about-babywraps/">What is a BubbaCosy&trade;?</a></li>
											<li><a href="/about-babywraps/how-to-use.php">As Easy as Steps 1, 2, 3!</a></li>
											<li><a href="/about-babywraps/benefits.php">Why choose BubbaCosy?</a></li>
											<li><a href="/about-babywraps/care.php">Care Instructions</a></li>
											<li><a href="/about-babywraps/history.php">The BubbaCosy&trade; Story</a></li>
										</ul>
									</li>
									<li id="nav-people"><a href="/baby-safe-sleep-testimonials.php" title="Find out what people are saying about our product"><span>What people</span> are saying?</a></li>
									<li id="nav-gallery"><a href="/baby-swaddle-gallery.php" title="View the BubbaCosy in action"><span>The wrap</span> Gallery</a></li>
									<li id="nav-stockists"><a href="/where-to-buy-baby-wraps-blankets.php" title="Find out where to buy your Bubbacosy"><span>Stockists</span> List</a></li>
									<li id="nav-shop"><a href="/baby-wrap-products.php" title="Shop Online for your own BubbaCosy now!"><span>Shop</span> Now!</a></li>
								</ul>
								<div class="clear"></div>
							</div>
							
						<!--//End Navigation-->
					
					</div>
				</div>
			</div>