	<script type="text/javascript" src="lib/jquery-core.js"></script>
	<script type="text/javascript" src="js/jquery-form.js"></script>
	<script type="text/javascript" src="js/jquery-superfish.js"></script>
	<script type="text/javascript" src="js/jquery-pikachoose.js"></script>
	<script type="text/javascript" src="js/jquery-fancybox.js"></script>
	<script type="text/javascript" src="js/jquery-wiggle.js"></script>
    <script type="text/javascript" src="js/responsiveslides.min.js"></script>
	<script type="text/javascript" src="js/jquery-config.js"></script>
	
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-32037239-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
