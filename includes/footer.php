	<div id="frame-footer">
		<div id="footer-contact">
			<div id="footer-container" class="wrapper">
				<div id="navigation-footer" class="float-left">
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/faq.php" title="Questions? We have answers on our FAQ">FAQ</a></li>
						<li class="end"><a href="/payments.php" title="Read about our payments and shipping policies here">Payments &amp; Shipping</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div id="expand-button" class="float-left">
					<h2 class="float-left"><span>Contact</span> our team</h2>
					<span id="icon-plus" class="plus float-right">Expand/contract</span>
					<div class="clear"></div>
				</div>
				<div id="shout" class="float-right">
					<a href="http://www.blinkcreative.com.au" target="_blank" title="Love our site? Blink Creative designed and developed it">Digital Agency Melbourne</a>
				</div>
				<div class="clear"></div>
			</div>
			<div id="contact-panel">
				<div id="contact-information">
					<p>Our team of little birdies are happy to hear from you regarding any queries or feedback that you may have. Fill out the fields to the right and we will get back to you shortly.</p>
					<p>If you would prefer to speak directly with one of our lovely birdies, you can do so via the methods below:</p>
					<dl>
						<dt>Phone:</dt>
						<dd>0402 256 670</dd>
						<dt>E-Mail:</dt>
						<dd><a href="mailto:info@bubbacosy.com.au">info@bubbacosy.com.au</a></dd>
					</dl>
				</div>
				<div id="contact-form" class="float-left">
					<form id="form" action="/form/process.php" method="post">
						<fieldset>
							<div id="input-fields" class="float-left">
								<label for="Name">Full Name *</label>
								<input id="Name" type="text" name="FullName" />
								<label for="Phone">Phone</label>
								<input id="Phone" type="text" name="Phone" />
								<label for="Email">E-Mail *</label>
								<input id="Email" type="text" name="EmailAddress" />
							</div>
							<div id="enquiry-fields" class="float-right">
								<label for="Message">Enquiry *</label>
								<textarea id="Message" name="Enquiry" cols="20" rows="20"></textarea>
								<div id="email-message" class="float-left">
									<div class="success">E-Mail sent!</div>
									<div class="error">E-Mail error!</div>
								</div>
								<div class="float-right">
									<button type="submit">Submit form</button>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</fieldset>
					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46307107-4', 'auto');
  ga('send', 'pageview');

</script>