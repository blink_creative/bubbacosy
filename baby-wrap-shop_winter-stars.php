<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Winter Wraps | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

<!--Facebook SDK-->

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

</head>

<body class="shop">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-shop" class="wrapper">
				<h1><span class="logo-genericwhite">BubbaCosy</span> <span class="title">All Season Wrap</span></h1>
				<div id="custom-button"><a href="mailto:info@bubbacosy.com.au?Subject=Custom%20made%20BubbaCosy%20enquiry">Enquire about custom bubbawraps</a></div>
				<div class="clear"></div>
				<div id="shop-container">
					<div id="product" class="float-left">
						<div id="product-viewer">
							<div id="product-social">
								<div id="facebook" class="float-left">
									<div class="fb-like" data-send="false" data-width="450" data-show-faces="false" data-font="arial"></div>
								</div>
								<div id="networks" class="float-right">
									<div id="twitter" class="float-left"><a href="https://twitter.com/share">Follow us on Twitter</a></div>
									<div id="email" class="float-left"><a href="mailto:Your friends email?Subject=Check%20out%20this%20cute%20baby%20wrap%20from%20BubbaCosy%21&Body=I%20just%20found%20the%20cutest%20wrap%20on%20bubbacosy.com%21%20%0A%0AClick%20on%20the%20link%20below%20to%20have%20a%20look%3A%0Ahttp%3A//www.bubbacosy.com.au/shop.php">E-Mail a friend</a></div>
								</div>
								<div class="clear"></div>
							</div>
							<ul id="product-image">
								<li><img src="images/shop/winter-stars/shop-thumb-three.jpg" ref="images/shop/winter-stars/shop-image-three.jpg" alt="Thumbnail One" width="75" height="75" /></li>
								<li><img src="images/shop/winter-stars/shop-thumb-four.jpg" ref="images/shop/winter-stars/shop-image-four.jpg" alt="Thumbnail Four" width="75" height="75" /></li>
								<li><img src="images/shop/winter-stars/shop-thumb-two.jpg" ref="images/shop/winter-stars/shop-image-two.jpg" alt="Thumbnail Three" width="75" height="75" /></li>
								<li><img src="images/shop/winter-stars/shop-thumb-five.jpg" ref="images/shop/winter-stars/shop-image-five.jpg" alt="Thumbnail Four" width="75" height="75" /></li>
								<li><img src="images/shop/winter-stars/shop-thumb-one.jpg" ref="images/shop/winter-stars/shop-image-one.jpg" alt="Thumbnail Two" width="75" height="75" /></li>
							</ul>
							<h3>Click to change view</h3>
						</div>
					</div>
					<div id="product-details" class="float-left">
						<div id="title">
						</div>
						<div id="details">
							<h3>The Smart Baby Wrap (Starry Night)</h3>
							<ul id="product-notes">
								<li>Suits 0 - 5 months</li>
								<li>Comfortable and Safe</li>
								<li>Keeps baby warm</li>
								<li>Promotes longer sleep</li>
								<li>Grows with your baby</li>
								<li>Keeps baby secure</li>
								<li>Fits in prams, cradles, cots and bassinets</li>
								<li>Machine washable</li>
								<li>Low Fire Danger</li>
								<li>Cover: 80% Cotton, 20% Polyester</li>
								<li>Duvet Filling: 375g 100% Australian Washable Wool</li>
								<li>Duvet Outer: 100% Japara Cotton</li>
							</ul>
						</div>
						
						
						<div id="prices">
							<h2>Australian Buyers <span>(includes shipping)</span></h2>
							<div id="price" class="float-left">
								<h3>$99.00<span>AUD</span></h3>
							</div>
							<div class="addtobag" class="float-right ">
								<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<input type="hidden" name="cmd" value="_s-xclick">
									<input type="hidden" name="hosted_button_id" value="DVX9KH56QAH72">
									<button type="submit" border="0" name="submit">Add to bag</button>
									<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
								</form>
							</div>
							<div class="clear"></div>
							<h2>International Buyers <span>(includes shipping)</span></h2>
							<div id="price" class="float-left">
								<h3>$124.00<span>AUD</span></h3>
							</div>
							<div class="addtobag" class="float-right ">
								<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<input type="hidden" name="cmd" value="_s-xclick">
									<input type="hidden" name="hosted_button_id" value="R7LS3TZ6LHR6C">
									<button type="submit" border="0" name="submit">Add to bag</button>
									<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
								</form>
							</div>
							<div class="clear"></div>
						</div>
						
					</div>
					<div class="clear"></div>
				</div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
