<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Legal | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis beige">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content" class="wrapper">
				<h1><span>What is a</span> BubbaCosy Wrap?</h1>
				<div id="content-copy" class="float-left">
					<h2>The smart baby wrap</h2>
					<p>BubbaCosy has been designed to give your newborn baby the security and comfort that only a mum can give. Made from the finest quality natural fabrics it is soft and gentle on baby's skin.</p>
					<h2>2-in-1</h2>
					<p>BubbaCosy has been designed to give your newborn baby the security and comfort that only a mum can give. Made from the finest quality natural fabrics it is soft and gentle on baby's skin.</p>
					<h2>BubbaCosy grows with your baby!</h2>
					<p>Thanks to the easily adjustable length and grip tabs located on the wings of the wrap, BubbaCosy can be used for babies up to 4–5 months.</p>
				</div>
				<div class="image-tree float-right"></div>
				<div class="clear"></div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
