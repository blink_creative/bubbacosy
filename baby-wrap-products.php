<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Wraps All Seasons | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="shop beige">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content" class="wrapper">
				<h1><span>View our range of</span> seasonal baby wraps</h1>
				<div id="product-range">
					<ul>
						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_winter.php"><img src="images/shop-product-winter.jpg" alt="Winter Product"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_winter.php">All Season Wrap - The Original</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The BubbaCosy 'all season' wrap is designed for cool to mild climates to give your newborn baby the security and comfort that only a mum can give. With the finest quality natural fabrics including a duvet made from 100% Australian washable wool.</p>
								<a class="button" href="/baby-wrap-shop_winter.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_winter-dots.php"><img src="images/shop-product-winter-dots.jpg" alt="Winter Product"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_winter-dots.php">All Season Wrap - Hokey Pokey</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The BubbaCosy 'all season' wrap is designed for cool to mild climates to give your newborn baby the security and comfort that only a mum can give. With the finest quality natural fabrics including a duvet made from 100% Australian washable wool.</p>
								<a class="button" href="/baby-wrap-shop_winter-dots.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_winter-stars.php"><img src="images/shop-product-winter-stars.jpg" alt="Winter Product"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_winter-stars.php">All Season Wrap - Starry Night</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The BubbaCosy 'all season' wrap is designed for cool to mild climates to give your newborn baby the security and comfort that only a mum can give. With the finest quality natural fabrics including a duvet made from 100% Australian washable wool.</p>
								<a class="button" href="/baby-wrap-shop_winter-stars.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_summer.php"><img src="images/shop-product-summer.jpg" alt="Summer Product"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_summer.php">Light Wrap - Summer Hoot</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The BubbaCosy 'light' wrap is designed for mild to warm climates or summer and indoor use to keep your newborn baby safe and comfortable while at home or on the go. With the finest quality natural fabrics including a duvet made from 100% cotton.</p>
								<a class="button" href="/baby-wrap-shop_summer.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_combo.php"><img src="images/shop-product-combo.jpg" alt="All season and light wraps"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_combo.php">Wrap Combo - Summer Hoot &amp; The Original</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The Bubbacosy combination pack includes the all season and light wrap to keep your special bundle of joy cosy and safe all year round through chilly winters and the warmer spring and summer months.</p>
								<a class="button" href="/baby-wrap-shop_combo.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_combo-dots.php"><img src="images/shop-product-combo-dots.jpg" alt="All season and light wraps"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_combo-dots.php">Wrap Combo - Summer Hoot &amp; Hokey Pokey</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The Bubbacosy combination pack includes the all season and light wrap to keep your special bundle of joy cosy and safe all year round through chilly winters and the warmer spring and summer months.</p>
								<a class="button" href="/baby-wrap-shop_combo-dots.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<div class="product-image float-left">
								<a href="/baby-wrap-shop_combo-stars.php"><img src="images/shop-product-combo-stars.jpg" alt="All season and light wraps"/></a>
							</div>
							<div class="product-information float-left">
								<h2><a href="/baby-wrap-shop_combo-stars.php">Wrap Combo - Summer Hoot &amp; Starry Night</a></h2>
								<h3>The Smart Baby Wrap</h3>
								<p>The Bubbacosy combination pack includes the all season and light wrap to keep your special bundle of joy cosy and safe all year round through chilly winters and the warmer spring and summer months.</p>
								<a class="button" href="/baby-wrap-shop_combo-stars.php">View Product</a>
							</div>
							<div class="clear"></div>
						</li>						
					</ul>
				</div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
