<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Payment and Shipping | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis beige">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content" class="wrapper">
				<h1><span>Thank You</span></h1>
				<div id="content-copy" class="float-left">
					<p>Your payment has been received, thank you for purchasing with BubbaCosy.</p>
				</div>
				<div class="image-tree float-right"></div>
				<div class="clear"></div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
