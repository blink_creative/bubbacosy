<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Swaddles Photos | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<link rel="stylesheet" type="text/css" href="css/fancybox.css" />

<!--//End CSS-->

</head>

<body class="gallery beige page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-gallery">
				<div class="wrapper">
					<h1><span>Gallery of precious</span> BubbaCosy Moments</h1>
					<h2>Click frame to view</h2>
					<div id="gallery">
						<ul>
							<li id="thumb-one"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-one.jpg">Thumbnail 1</a></li>
							<li id="thumb-two"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-two.jpg">Thumbnail 2</a></li>
							<li id="thumb-three"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-three.jpg">Thumbnail 3</a></li>
							<li id="thumb-four"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-four.jpg">Thumbnail 4</a></li>
							<li id="thumb-five"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-five.jpg">Thumbnail 5</a></li>
							<li id="thumb-six"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-six.jpg">Thumbnail 6</a></li>
							<li id="thumb-seven"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-seven.jpg">Thumbnail 7</a></li>
							<li id="thumb-eight"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-eight.jpg">Thumbnail 8</a></li>
							<li id="thumb-nine"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-nine.jpg">Thumbnail 9</a></li>
							<li id="thumb-ten"><a class="lightbox" rel="next" href="images/gallery/fullimage/full-ten.jpg">Thumbnail 10</a></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Swaddles</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% Australian washable wool doona inside, Bubbacosy keeps your baby cosy and warm whilst providing the comfort of a gentle swaddle. </p>
				</div>
				<div class="float-left">
					<img src="images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
