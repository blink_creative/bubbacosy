<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Payment and Shipping | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis beige">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content" class="wrapper">
				<h1><span>Payments</span>&amp; Shipping.</h1>
				<div id="content-copy" class="float-left">
					<h2>Refund / Exchange Policy</h2>
					<p>We hope you will be completely happy with your purchase. However if for any reason you need to return goods, please do so within 15 days, unused with all packaging intact and we will either exchange them or refund you.</p>
					<p>Please return all goods with your receipt and reason for refund/exchange to the following address:<br/>
					<strong>BubbaCosy, 23 Maxwell St, Lalor 3075, Victoria, Australia</strong></p>
					<p>We are unable to refund postage costs for returns, unless we have made a mistake and sent you the wrong product or the product is unusable due to an imperfection.</p>
					<p>Exchanges that are not the result of an imperfection or a mistake where BubbaCosy has sent you the wrong product will require additional postage and handling charges. $10 for Australian Customers and $12.50 for New Zealand.</p>
					<p>International customers will be contacted individually to arrange additional return postage costs.<br/>
					If you have any queries in relation to our service or products, or have misplaced your receipt, please contact us via email and we will get back to you as soon as possible. We will aim to respond to all queries within 48 hours. Once we have contacted you, we will keep you informed of the progress of your queries and aim to have it resolved within 5 working days.</p>
				</div>
				<div class="image-tree float-right"></div>
				<div class="clear"></div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
