<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Wraps FAQ | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="testimonials beige">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content" class="wrapper">
				<h1><span>Frequently Asked</span> Questions</h1>
				<div id="content-copy" class="float-left">
					<ul>
						<li>
							<h2>Why choose BubbaCosy?</h2>
							<p>Our specially designed BubbaCosy has a woollen duvet inside which keeps baby warm, supports baby's neck, head and posture. It prolongs baby’s sleep. Our BubbaCosy was designed with your baby’s safety and comfort in mind. </p>
						</li>
						<li>
							<h2>How do I use the baby wrap?</h2>
							<p>In three easy steps your baby can be snug and warm. It's super easy! Refer to pictures in our brochure.</p>
						</li>
						<li>
							<h2>How long can I use BubbaCosy for?</h2>
							<p>Our BubbaCosy, the smart baby wrap, is designed to grow with your baby. Most mums will use the smart baby wrap for the first five months of the baby's life.</p>
						</li>
						<li>
							<h2>Is the baby wrap only to be used for sleeping time?</h2>
							<p>No! This is a multipurpose designed product with your newborn baby’s needs in mind. This is a 3 in 1 product often used by mums as a wrap, change mat, and play mat.</p>
						</li>
						<li>
							<h2>Is the fabric safe for my baby's skin?</h2>
							<p>Our BubbaCosy, the smart baby wrap was always designed with your newborn baby in mind. The fabric chosen is gentle on baby's skin, breathable, and fire retardant. Our carefully chosen duvet is made from 100% Australian washable wool, which is moisture absorbent, anti bacterial and allergy free.</p>
						</li>
						<li>
							<h2>Do I need to use additional blankets? </h2>
							<p>No, each Bubbacosy has a woollen duvet inside the cover which helps to regulate baby's core temperature, eliminating the need for additional blankets. </p>
						</li>
						<li>
							<h2>Can my baby overheat in BubbaCosy?</h2>
							<p>No! Babies control their temperature predominantly through the face. Our BubbaCosy encourages sleeping baby on the back with the head and face uncovered. Your baby only needs a nappy and Singlet in warmer weather, or a lightweight JUMP suit in cooler weather.</p>
						</li>
						<li>
							<h2>How do I care for my baby wrap?</h2>
							<p>Both cover and duvet –insert, are machine washable. For best results take the duvet out of the cover, turn cover inside out and wash separately. For detailed instructions, refer to the care label on duvet and cover.</p>
						</li>
					</ul>
				</div>
				<div class="image-sing float-right">
					<div class="double-note note-one wiggle"></div>
					<div class="single-note note-two wiggle"></div>
					<div class="single-note note-three wiggle"></div>
					<div class="single-note note-four wiggle"></div>
					<div class="double-note-small note-five wiggle"></div>
				</div>
				<div class="clear"></div>
			</div>
		
		<!--//End Content-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
