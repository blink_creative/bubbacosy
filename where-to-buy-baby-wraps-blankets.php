<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Blankets, Where to Buy | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/layout.css" />

<!--//End CSS-->

</head>

<body class="stockists beige page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-stockists">
				<div class="wrapper">
					<h1><span>Where can I buy a</span> BubbaCosy Wrap?</h1>
					<div id="bird-large"></div>
					<div id="eye-left" class="eye"></div>
					<div id="eye-right" class="eye"></div>
					<ul id="stockists-list">
						<li id="stockist-vic">
							<h2>Victoria</h2>
							<dl>
								<dt>A Kiddie Country</dt>
								<dd>79/83 Union Street<br/>Armadale  Vic  3143<br/>Phone: 9509 4041<br/><a target="_blank" href="http://www.kiddiecountry.com.au">www.kiddiecountry.com.au</a></dd>
							</dl>
							<dl>
								<dt>Big Dreams - Collingwood</dt>
								<dd>336 Smith Street<br/>Collingwood VIC 3066<br/>Phone: 9078 9438<br/><a target="_blank" href="http://www.bigdreams.com.au">www.bigdreams.com.au</a></dd>
							</dl>
							<dl>
								<dt>Big Dreams - Northcote</dt>
								<dd>269 High Street<br/>Northcote VIC 3070<br/>Phone: 9489 2193<br/><a target="_blank" href="http://www.bigdreams.com.au">www.bigdreams.com.au</a></dd>
							</dl>
							<dl>
								<dt>Coochicoo - Malvern</dt>
								<dd>Shop 27/28,<br/>Malvern Central Shopping Centre<br/>Malvern Vic 3144<br/>Phone: 9509 7744<br/><a href="http://www.coochicoo.com.au">www.coochicoo.com.au</a></dd>
							</dl>
							<dl>
								<dt>Coochicoo - Brighton</dt>
								<dd>57 Church Street<br/>Brighton  Vic  3186<br/>Phone: 9530 5566<br/><a href="http://www.coochicoo.com.au">www.coochicoo.com.au</a></dd>
							</dl>
						</li>
						<li id="stockist-nsw">
							<dl>
								<dt>Coochicoo - Camberwell</dt>
								<dd>Shop 118/119, <br/>The Well @ Camberwell<br/>793 Burke Rd<br/>Camberwell  Vic  3124<br/>Phone: 9813 4411<br/><a href="http://www.coochicoo.com.au">www.coochicoo.com.au</a></dd>
							</dl>
							<h2>New Zealand</h2>
							<dl>
								<dt>Cariboo New Zealand</dt>
								<dd>11 High St<br/>RANGIORA 7400<br/>PH +64 3 313 4329</dd>
							</dl>
						</li>
						<li id="stockist-qld">
							<h2>People’s Republic of China</h2>
							<dl>
								<dt>Sunshine Baby</dt>
								<dd>Yunnan</dd>
							</dl>
							<dl>
								<dt>Sunshine Baby</dt>
								<dd>Kunming<br/>Yunnan province</dd>
							</dl>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Blankets</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% Australian washable wool doona inside, Bubbacosy is as snuggly and warm as a blanket and with easily adjustable length and grip tabs provides the comfort of a gentle swaddle.</p>
				</div>
				<div class="float-left">
					<img src="images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
