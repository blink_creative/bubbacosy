<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Blankets Benefits | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis green page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('../includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>Why choose</span> BubbaCosy Wraps?</h1>
					<div id="why-content" class="float-left">
						<ul id="why-bullets">
							<li>The multi-purpose design functions as a wrap, duvet, change mat and play mat.</li>
							<li class="alt-bullet">Baby is always snug and secure, encouraging a longer lasting sleep.</li>
							<li>The adjustable design ensures a cosy fit for babies of all shapes and sizes.</li>
							<li class="alt-bullet">Made with natural fabrics that are gentle on baby's skin.</li>
							<li>Conveniently fits in prams, cradles, cots and bassinets.</li>
							<li class="alt-bullet">Machine washable and easy to maintain.</li>
							<li>Safe, quick and easy to use.</li>
							<li class="alt-bullet">100% Australian washable wool.</li>
						</ul>
						<div id="why-image">
							<h2 class="gear-one">inner <span>wrap</span></h2>
							<div class="gear gear-green-one"></div>
							<h2 class="gear-two"><span>Duvet</span> outer</h2>
							<div class="gear gear-green-two"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Blankets</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% Australian washable wool doona inside, Bubbacosy is as snuggly and warm as a blanket and with easily adjustable length and grip tabs provides the comfort of a gentle swaddle.</p>
				</div>
				<div class="float-left">
					<img src="../images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('../includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('../includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
