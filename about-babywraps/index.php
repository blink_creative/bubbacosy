<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Grow Bags | BubbaCosy</title>
<meta name="keywords" content="baby, wrap, australia, swaddle, blankets, grow bags" />
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis beige page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('../includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>What is a</span> BubbaCosy Wrap?</h1>
					<div id="content-copy" class="float-left">
						<h2>The smart baby wrap</h2>
						<p>BubbaCosy has been designed to give your newborn baby the security and comfort that only a mum can give. Made from the finest quality natural fabrics it is soft and gentle on baby's skin.</p>
						<h2>2-in-1</h2>
						<p>Each BubbaCosy™ has a duvet inside to support posture and provide the extra warmth your baby needs. The duvet, made from 100% Australian washable wool, helps to regulate baby’s body temperature. It’s breathable, flame-retardant and moisture absorbing qualities makes it ideal for even the most sensitive skin.</p>
						<h2>BubbaCosy grows with your baby!</h2>
						<p>Thanks to the easily adjustable length and grip tabs located on the wings of the wrap, BubbaCosy can be used for babies up to 4–5 months.</p>
					</div>
					<div class="image-tree float-right">
						<div class="bird-one">
						
						</div>
						<div class="bird-two">
						
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Grow Bags</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>From cradle to pram to your loving arms, Bubba Cosy is the only baby wrap with a washable wool doona inside, ideal for sleep or adventures!</p>
				</div>
				<div class="float-left">
					<img src="../images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('../includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('../includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
