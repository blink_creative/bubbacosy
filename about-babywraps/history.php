<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Swaddles | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis brown history page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('../includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>How it all began</span> Many Moons Ago</h1>
					<div id="content-copy" class="float-left">
						<p>My name is Ivana Mostafa. Born in former Czechoslovakia, I arrived to Australia in early 1986 where I went on to raise two daughters. Today, I am the proud grandmother of three!</p>
						<p>The idea to create a baby wrap came to me after the birth of my first granddaughter in 2007. Like most ‘grandmas’, I wanted to make life a little easier for my daughter. Watching the next generation begin the precious journey of motherhood, I reminisced about taking my own daughter home from hospital wearing a special baby wrap that my mother had bought me. For the next four months my daughter literally lived in that wrap! I began searching for something similar but quickly discovered there was nothing on the market. With new parents in mind (and remembering my first clumsy weeks as a new mum) I wanted to offer something safe for my grandchild; something warm and comfortable that would help support her fragile body and head.</p> 
						<p>I have always loved sewing so felt inspired to instead create my own baby wrap. When I brought it to the hospital, it was an instant success! Even the nurses were asking me where they could buy one. This was how the Smart Baby Wrap was born. Word travelled fast as my daughter’s friends (and friends of friends!) began requesting a unique baby wrap to call their own. The business grew quickly and with every order, my humble baby wrap evolved to combine aesthetic beauty and functionality to better suit the modern mum.</p>
						<p>Once I began selling online, the response was tremendous! Everyone wanted to know why the baby wraps weren’t available in stores. So in early 2012 I took the plunge and presented the re-branded baby wrap “BubbaCosy” to Australian retail.</p>
					</div>
					<div class="image-moon float-right"></div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Swaddles</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% Australian washable wool doona inside, Bubbacosy keeps your baby cosy and warm whilst providing the comfort of a gentle swaddle. </p>
				</div>
				<div class="float-left">
					<img src="../images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>

<!--//Footer-->

	<?php include('../includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('../includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
