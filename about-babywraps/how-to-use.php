<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Sleeping Bags | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis orange page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('../includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>It's as easy as</span> Steps 1, 2, 3!</h1>
					<ul id="steps">
						<li id="step-one">
							<h2>Step 1</h2>
							<div class="gear"></div>
							<div class="baby-image"></div>
							<p>Unfold the BubbaCosy and position baby so the head rests comfortably within the arch.</p>
						</li>
						<li id="step-two">
							<h2>Step 2</h2>
							<div class="gear"></div>
							<div class="baby-image"></div>
							<p>Fold bottom flap over baby's body, allowing room for legs to wriggle and extend comfortably.</p>
						</li>
						<li id="step-three">
							<h2>Step 3</h2>
							<div class="gear"></div>
							<div class="baby-image"></div>
							<p>Gently wrap the left side wing around the front of baby and secure with the right wing using the grip tabs. Adjust as tight or loosely as required.</p>
						</li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Sleeping Bags</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>From cot to pram to your loving arms, Bubba Cosy is the only baby wrap with a washable wool doona inside, making it perfect for sleeping or being out and about.</p>
				</div>
				<div class="float-left">
					<img src="../images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('../includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('../includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
