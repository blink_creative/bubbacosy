<!DOCTYPE HTML>
<html>
<head>

<!--//Meta-->

<title>Baby Wraps Care | BubbaCosy</title>
<meta name="description" content="The smart baby wrap by BubbaCosy is designed to swaddle your newborn baby with the security and comfort that only a mum can give. The multi-purpose design acts as a wrap, duvet, change mat &amp; play mat." />
<meta charset="utf-8">

<!--//End Meta-->

<!--//CSS-->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="../css/layout.css" />

<!--//End CSS-->

</head>

<body class="whatis beige page-comparison">

<div id="site-wrapper">
	<div id="main-content">

		<!--//Header-->
			
			<?php include('../includes/header.php'); ?>
		
		<!--//End Header-->
		
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
					<h1><span>How to care for</span> your BubbaCosy</h1>
					<div id="content-copy" class="float-left">
						<p>Both the cover and duvet are machine washable. For the best results take the duvet out of the cover and wash separately.</p>
						<p>To protect the fabric’s texture, turn the cover inside out before washing.</p>
						<p>Set machine wash to gentle/delicate spin cycle in either warm or cold water. After wash is complete, hang both the duvet and cover out to dry.</p>
						<p>Both the cover and duvet are iron friendly. For the best results, use a steam iron and set the dial to a moderate to high temperature setting.</p>
						<p>Important: Do not bleach or tumble dry. Do not dry clean.</p>
					</div>
					<div class="image-powerline float-right">
						<div class="sheet">
							<img src="../images/clothing.jpg" alt="Clothes" />
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="border-bottom"></div>
			</div>
		
		<!--//End Content-->
		
        <!--//Comparison-->
		
        <div class="comparison">
			<div class="wrapper">
				<div class="float-left copy">
					<h2>Baby Wraps</h2>
					<h3><span>VS.</span>Bubbacosy</h3>
					<p>With a 100% wool doona inside, Bubba Cosy baby wraps take your baby straight from cot to pram. No need to unwrap or take extra blankets on your outing. </p>
				</div>
				<div class="float-left">
					<img src="../images/baby.png" alt="Baby Image"/>
				</div>
				<div class="clear"></div>
			</div>
        </div>
		
        <!--//End Comparison-->
		
	</div>
</div>
		
<!--//Footer-->

	<?php include('../includes/footer.php'); ?>

<!--//End Footer-->


<!--//Scripts-->

	<?php include('../includes/script.php'); ?>

<!--//End Scripts-->


</body>
</html>
