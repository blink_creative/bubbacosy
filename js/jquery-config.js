$(document).ready(function() {
	console.log("inittest");
	/*--rotating ears--*/
	
	$(function() {
		var $elie = $('.gear','#steps'), degree = 0, timer;
		rotate();
		function rotate() {
	
			  // For webkit browsers: e.g. Chrome
			$elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
			  // For Mozilla browser: e.g. Firefox
			$elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
	
			  // Animate rotation with a recursive call
			timer = setTimeout(function() {
				++degree; rotate();
			},12);
		}
	
	});
	
	$(function() {
		var $elie = $('.gear','#why-image'), degree = 0, timer;
		rotate();
		function rotate() {
	
			  // For webkit browsers: e.g. Chrome
			$elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
			  // For Mozilla browser: e.g. Firefox
			$elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
	
			  // Animate rotation with a recursive call
			timer = setTimeout(function() {
				++degree; rotate();
			},12);
		}
	
	});
	
	$(function() {
		var $elie = $('#rotating-gear','#splash-gear'), degree = 0, timer;
		rotate();
		function rotate() {
	
			  // For webkit browsers: e.g. Chrome
			$elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
			  // For Mozilla browser: e.g. Firefox
			$elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
	
			  // Animate rotation with a recursive call
			timer = setTimeout(function() {
				++degree; rotate();
			},16);
		}
	
	});
	
	/*--Wiggling notes--*/
	
	$(function() {
		var interval = null;
		$('.wiggle').wiggle();
	});
	
	/*--jumping birds--*/
	
	animateTheBox();
	function animateTheBox() {
		$(".bird-one").animate({ "bottom": "+=10px" }, 400);
		$(".bird-one").animate({ "bottom": "-=10px" }, 600, animateTheBox);
		$(".bird-two").animate({ "bottom": "+=5px" }, 200);
		$(".bird-two").animate({ "bottom": "-=5px" }, 400, animateTheBox);
    };
	
	bigBird();
	function bigBird() {
		$("#bird-large").animate({ "top": "-=20px" }, 600);
		$("#bird-large").animate({ "top": "+=20px" }, 300, bigBird);
    };
	
	eyeBird();
	function eyeBird() {
		$("#eye-left").animate({ "top": "-=10px","left":"+=10px" }, 600);
		$("#eye-left").animate({ "top": "+=10px","left":"-=10px" }, 300, eyeBird);
		$("#eye-right").animate({ "top": "-=10px","left":"+=10px" }, 600);
		$("#eye-right").animate({ "top": "+=10px","left":"-=10px" }, 300, eyeBird);
    };
	
	animateTheSheet();
	function animateTheSheet() {
		$(".sheet").animate({ "left": "-=290px","top": "-=55px" }, 3000);
		$(".sheet").animate({ "left": "+=290px", "top": "+=55px" }, 2500, animateTheSheet);
    };


	/*--Superfish--*/
	
	$("ul.sf-menu").superfish({
		autoArrows:false	
	}); 
 
	/*--Footer fly--*/
	
	$('#expand-button').click(function() {
		$('#icon-plus').toggleClass('plus');
		$('#icon-plus').toggleClass('minus');
	});
	
	$(function($) {
		var slide = false;
		var height = $('#contact-panel').height();
		$('#expand-button').click(function() {
			var docHeight = $(document).height();
			var windowHeight = $(window).height();
			var scrollPos = docHeight - windowHeight + height;
			$('#contact-panel').animate({ height: "toggle"}, 1000);
			if(slide == false) {
				if($.browser.opera) { //Fix opera double scroll bug by targeting only HTML.
					$('html').animate({scrollTop: scrollPos+'px'}, 1000);
				} else {
					$('html, body').animate({scrollTop: scrollPos+'px'}, 1000);
				}
								   slide = true;
			} else {
								   slide = false;
						   }
		});
	});
	
	/*--contact validation--*/
	
	$('#form').ajaxForm(function(data) {
		if (data==0){$('.error').hide();$('.success').fadeIn("slow");$('#contact-form form').resetForm();}
		else if (data==1){$('.success').hide(); $('.error').fadeIn("slow");}
	});
	
	/*--Shop Image Chooser--*/
	
	$("#product-image").PikaChoose({
		thumbOpacity:0.6
	});
	
	/*--Gallery Lightbox--*/

	$("a.lightbox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200
	});

	    /*--Animations--*/

    var why = ('.why');

    $(why).hover(function() {
        $('.why-image').toggleClass("tossing");
    });

    var shop = ('.shop');

    $(shop).hover(function() {
        $('.shop-image').toggleClass("floating");
    });

    /*--Slider--*/

    $(function() {
        $(".slides").responsiveSlides({
            timeout: 6000
        });
    });

    $('body').removeClass('preload');
	
	/*-- GOOGLE ANALYTICS ADD SHOPPING ITEM --*/
	
	function gaAddCart() {
  		ga('send', 'event', {
    		eventCategory: 'Add to cart',
    		eventAction: 'click',
    		eventLabel: 'Item Added'
  		});
	}
	
	$('#addtobag form').submit(function() {
		gaAddCart();
		console.log("testeraddbag");
	});
	
});
